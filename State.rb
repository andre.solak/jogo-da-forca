class State
  
  attr_accessor :player
  
  def initialize(n)
    @n = n
    @player = 0
    @body_parts = File.read("body_parts.txt").split(',')
    @p = 0
  end

  def which_player?()
    puts "\nÉ a vez do jogador #{@player+1}\n"
  end

  def show_body_parts()
    puts "#{@body_parts[@p]}\n"
  end

  def next()
    @p += 1
    @player = (@player+1) % @n
  end

  def end_game?()
    show_body_parts()
    return @p == 6
  end

  def restart_state()
    @p = 0
    @player = 0
  end

  def print_line()
    puts "--------------------------------------------------------------------------"
  end

end