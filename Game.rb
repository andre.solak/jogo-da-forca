require_relative "Movies"
require_relative "State"

class Game
  def initialize(n)
    @letters = []
    @all_words = Movies.new
    @word = "12 Homens e Uma Sentença"
    @hits = '0123456789!-?,.º :'
    @special_chars = initialize_special_chars()
    @game_state = State.new(n)
  end

  def show_blank_word()
    if @hits.empty?
      regex = /[\wáàâãéèêíïóôõöúçñÁÀÂÃÉÈÍÏÓÔÕÖÚÇÑº]/
    else
      regex = /[^#{@hits}\-]/
    end
    puts @word.gsub(regex, "_")
    puts "\nLetras usadas: ".concat(@letters.join(" ")) if !@letters.empty?
  end

  def start_round()
    @game_state.print_line()
    @game_state.which_player?()
    @game_state.show_body_parts()
  end

  def put_letter()
    puts "\nDigite um caractere ou a palavra correta"
    input = gets.chomp
    if input.length > 1
      is_word?(input)
    else
      input = repeated_letter?(input).downcase
      hit_letter?(@word, input)
    end
    if @game_state.end_game?()
      @hits = @word
      show_blank_word()
      puts "\nFim de jogo :("
      restart_game?()
    end
  end

  def repeated_letter?(letter)
    begin
      raise if @letters.join().include?(letter)
    rescue
      puts "Esta letra já foi digitada! Por favor, tente outra."
      letter = gets.chomp
      retry
    end
    return letter
  end

  def hit_letter?(word, letter)
    if normalize_word(word).include?(letter)
      @special_chars.values.each { |regex| letter = regex.inspect.gsub(/[\[\]\/]/, "") if letter =~ regex }
      @hits.concat(letter+letter.upcase)
      hit = true
      puts @hits
      puts normalize_word(word)
      normalize_word(word).each_char { |c|
        hit = false if !@hits.include?(c)
      }
      hit_word(word) if hit
    else
      @letters << letter
      @game_state.next()
    end        
  end

  def is_word?(word)
    if word.downcase == @word.downcase
      hit_word(@word)
    else
      puts "Palavra errada :( "
      @game_state.next()
    end
  end

  def hit_word(word)
    @game_state.print_line()
    @game_state.show_body_parts()
    @hits = word
    show_blank_word()
    puts "\n~~~~~ #{@game_state.player+1} vence! ~~~~~\n"
    restart_game?()
  end

  def restart_game?()
    puts "\nContinuar a jogar? [s/n]"
    begin
      restart = gets.chomp
      raise if restart != 's' and restart != 'n'
    rescue
      puts "A resposta que você digitou não é válida. Por favor, digite 's' para sim ou 'n' para não"
      retry
    end
    if restart == 's'
      @game_state.restart_state()
      @letters = []
      @word = @all_words.get_movie()
      @hits = '0123456789!-?,.º :'
    else
      exit
    end
  end

  def initialize_special_chars()
    special_chars = {}
    special_chars["a"] = /[aáàâã]/
    special_chars["e"] = /[eéèê]/
    special_chars["i"] = /[iíï]/
    special_chars["o"] = /[oóôõö]/
    special_chars["u"] = /[uú]/
    special_chars["n"] = /[nñ]/
    special_chars["c"] = /[cç]/
    return special_chars
  end

  def normalize_word(word)
    word_ = word.downcase
    @special_chars.each { |key, value|
      word_ = word_.gsub(value, key) if word_ =~ value
    }
    return word_
  end

end

content = File.read("title.txt")
puts "#{content}"
begin
  n = Integer(gets.chomp)
rescue
  puts "O número que você digitou não é válido. Por favor, tente de novo."
  retry
end

game = Game.new(n)

while 1 do
  game.start_round()
  game.show_blank_word()
  game.put_letter() 
end